-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- হোষ্ট: 127.0.0.1
-- তৈরী করতে ব্যবহৃত সময়: মার 07, 2020 at 05:11 PM
-- সার্ভার সংস্করন: 10.3.16-MariaDB
-- পিএইছপির সংস্করন: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- ডাটাবেইজ: `oop`
--

-- --------------------------------------------------------

--
-- টেবলের জন্য টেবলের গঠন `reg`
--

CREATE TABLE `reg` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(200) NOT NULL,
  `email` varchar(100) NOT NULL,
  `image` varchar(100) NOT NULL,
  `role` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- টেবলের জন্য তথ্য স্তুপ করছি `reg`
--

INSERT INTO `reg` (`id`, `name`, `username`, `password`, `email`, `image`, `role`) VALUES
(2, 'ravi@ravi.com', 'admin@admin.com', '', 'mamunur93@gmail.com', '200x80.png', ''),
(3, 'ravi@ravicom', 'admin@admin.com', '202cb962ac59075b964b07152d234b70', 'mamunur93@gmail.com', 'horror logo.png', ''),
(4, 'admin@gmail.com', 'root', '202cb962ac59075b964b07152d234b70', 'androidkotha@gmail.com', 'images.png', 'admin'),
(5, 'mamun@gmail.com', 'superadmin@gmail.com', '202cb962ac59075b964b07152d234b70', 'root@gmail.com', 'image_2019_12_20T14_51_15_974Z.png', 'admin'),
(6, 'admin@admin.com', 'mamun', 'e10adc3949ba59abbe56e057f20f883e', 'mamunur93@gmail.com', 'Array ', 'user'),
(7, 'mamunur93@gmail.com', 'admin', 'e10adc3949ba59abbe56e057f20f883e', 'mamunur93@gmail.com', '1579193451.png ', ''),
(8, 'sharmin', 'sharmin', '202cb962ac59075b964b07152d234b70', 'mamunur93@gmail.com', '1579366663.jpg ', '');

-- --------------------------------------------------------

--
-- টেবলের জন্য টেবলের গঠন `student`
--

CREATE TABLE `student` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- টেবলের জন্য তথ্য স্তুপ করছি `student`
--

INSERT INTO `student` (`id`, `name`, `email`) VALUES
(2, 'Mamunur Rahaman', 'admin@admin.com'),
(3, 'Waly mia', 'androidkotha@gmail.com'),
(4, 'Bkashi', 'admi1n@admin.com'),
(5, 'Mamunur Rahaman', 'mamunurrahamanbd@gm.com'),
(6, 'Istiyak amin', 'admin@admin.com'),
(7, 'Bkash', 'aaadmin@admin.com');

--
-- স্তুপকৃত টেবলের ইনডেক্স
--

--
-- টেবিলের ইনডেক্সসমুহ `reg`
--
ALTER TABLE `reg`
  ADD PRIMARY KEY (`id`);

--
-- টেবিলের ইনডেক্সসমুহ `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`);

--
-- স্তুপকৃত টেবলের জন্য স্বয়ক্রীয় বর্দ্ধিত মান (AUTO_INCREMENT)
--

--
-- টেবলের জন্য স্বয়ক্রীয় বর্দ্ধিত মান (AUTO_INCREMENT) `reg`
--
ALTER TABLE `reg`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- টেবলের জন্য স্বয়ক্রীয় বর্দ্ধিত মান (AUTO_INCREMENT) `student`
--
ALTER TABLE `student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
