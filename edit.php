<?php
include 'controller.php';
$db= new controller;
if (isset($_GET['id'])) {
	$id=$_GET["id"];
}
$sql="SELECT * FROM `student` WHERE id=".$id;
$data=$db->read($sql)->fetch_object();
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>



<div class="container">
  <h2>Horizontal form</h2>
  

  <div class="alert-danger">
    
  
<?php if(isset($error))
    {

    echo "<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>".$error;
  } ?>
</div>

    </span>
  <form class="form-horizontal" action="update.php" method="POST">
    <div class="form-group">
    	<input type="hidden" class="form-control" id="name" value="<?php echo $data->id; ?>"  name="id">
      <label class="control-label col-sm-2" for="name">Name:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="name" value="<?php echo $data->name; ?>"  name="name">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="email">Email:</label>
      <div class="col-sm-10">          
       <input type="email" class="form-control" id="email" value="<?php echo $data->email; ?>" name="email">
      </div>
    </div>
    
    <div class="form-group">        
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-default">Submit</button>
      </div>
    </div>
  </form>
</div>

</body>
</html>
