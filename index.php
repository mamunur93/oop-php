<?php
session_start();
include 'controller.php';
$user= new controller;

$uid = $_SESSION['uid'];

$sql="SELECT * FROM `student`";
$data=$user->read($sql);


if(!$user->session()){
  
  header("location:login.php");
  
}
if (isset($_GET['q'])){
        $user->logout();
        header("location:login.php");
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap-auto-dismiss-alert@1.0.2/bootstrap-auto-dismiss-alert.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Student Information</h2>
  <h3 style="float: right;"> <a href="index.php?q=logout">Logout</a></h3>

  <div class=" alert-success alert-dismissible fade in" data-auto-dismissrole="alert">
    
   <?php if(isset($_GET['msg']))
    {

    echo $_GET['msg'];
  } ?>


 
</div>
  
  <table class="table">
    <thead>
      <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Email</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      <?php if($data){
      while($row=$data->fetch_object()){
      ?>
      <tr>
        <td><?php echo $row->id; ?></td>
        <td><?php echo $row->name; ?></td>
        <td><?php echo $row->email; ?></td>
        <td><a class="btn btn-primary" href="edit.php?id=<?php echo $row->id; ?>" role="button">EDIT</a> <a class="btn btn-primary" href="delete.php?id=<?php echo $row->id; ?>" role="button">DELETE</a></td>
      </tr>      
      <?php }} else{echo "no data";} ?>
    </tbody>
  </table>
</div>

</body>
</html>

